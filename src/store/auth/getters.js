export function user (auth) {
  return auth.user
}

export function userId (auth) {
  return auth.userId
}

export function restaurantLists (auth) {
  return auth.restaurantsList
}

export function getUserIdToEdit (auth) {
  return auth.userIdToEdit
}
